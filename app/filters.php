<?php

/*
 * |-------------------------------------------------------------------------- 
 * | Application & Route Filters 
 * |-------------------------------------------------------------------------- 
 * | 
 * | Below you will find the "before" and "after" events for the application 
 * | which may be used to do any work before or after a request into your 
 * | application. Here you may also register your custom route filters. 
 * |
 */
App::before ( function ($request) {
	//
} );

App::after ( function ($request, $response) {
	//
} );

/*
 * |-------------------------------------------------------------------------- 
 * | Authentication Filters 
 * |-------------------------------------------------------------------------- 
 * | 
 * | The following filters are used to verify that the user of the current 
 * | session is logged into this application. The "basic" filter easily 
 * | integrates HTTP Basic authentication for quick, simple checking. 
 * |
 */

Route::filter ( 'auth', function () {
	if (Auth::guest ())
		return Redirect::guest ( 'users/login' );
} );

Route::filter ( 'auth.basic', function () {
	return Auth::basic ();
} );

/*
 * |-------------------------------------------------------------------------- 
 * | Guest Filter 
 * |-------------------------------------------------------------------------- 
 * | 
 * | The "guest" filter is the counterpart of the authentication filters as 
 * | it simply checks that the current user is not logged in. A redirect 
 * | response will be issued if they are, which you may freely change. 
 * |
 */

Route::filter ( 'guest', function () {
	if (Auth::check ())
		return Redirect::to ( '/book' );
} );

Route::filter ( 'administrator', function () {
	
	if (Auth::check ()) {
		
		$user = Auth::user ();
		
		if ($user->hasRole ( 'administrator' )) {
			return;
		} else {
			return Redirect::to ( 'users/login' )->with ( 'message', 'Your username/password combination was incorrect' )->withInput ();
		}
	} else {
		return Redirect::to ( 'users/login' )->with ( 'message', 'Your username/password combination was incorrect' )->withInput ();
	}
} );

Route::filter ( 'librarian', function () {
	if (Auth::check ()) {
		
		$user = Auth::user ();
		
		if ($user->hasRole ( 'librarian' )) {
			return;
		} else {
			return Redirect::to ( 'users/login' )->with ( 'message', 'Your username/password combination was incorrect' )->withInput ();
		}
	} else {
		return Redirect::to ( 'users/login' )->with ( 'message', 'Your username/password combination was incorrect' )->withInput ();
	}
} );

Route::filter ( 'member', function () {
	if (Auth::check ()) {
		
		$user = Auth::user ();
		
		if ($user->hasRole ( 'member' )) {
			return;
		} else {
			return Redirect::to ( 'users/login' )->with ( 'message', 'Your username/password combination was incorrect' )->withInput ();
		}
	} else {
		return Redirect::to ( 'users/login' )->with ( 'message', 'Your username/password combination was incorrect' )->withInput ();
	}
} );

Route::filter ( 'super_administrator', function () {
	if (Auth::check ()) {
		
		$user = Auth::user ();
		
		if ($user->hasRole ( 'super_administrator' )) {
			return;
		} else {
			return Redirect::to ( 'users/login' )->with ( 'message', 'Your username/password combination was incorrect' )->withInput ();
		}
	} else {
		return Redirect::to ( 'users/login' )->with ( 'message', 'Your username/password combination was incorrect' )->withInput ();
	}
} );

/*
 * |-------------------------------------------------------------------------- 
 * | 
 * CSRF Protection Filter 
 * |-------------------------------------------------------------------------- 
 * | 
 * | The CSRF filter is responsible for protecting your application against 
 * | cross-site request forgery attacks. If this special token in a user 
 * | session does not match the one given in this request, we'll bail. 
 * |
 */

Route::filter ( 'csrf', function () {
	if (Session::token () != Input::get ( '_token' )) {
		throw new Illuminate\Session\TokenMismatchException ();
	}
} );