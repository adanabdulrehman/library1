<?php

// class used to seed the authors table with data
// this is used for testing purposes
class AuthorSeeder extends Seeder {
	
	/**
	 * Run the genre seeds.
	 *
	 * @return void
	 */
	public function run() {
		// DB::table ( 'authors' )->delete ();
		Author::create ( array (
				'id' => '119',
				'name' => 'Zachary, Lois J.',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '118',
				'name' => 'Wilson, Geoff',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '117',
				'name' => 'Waterson, Merlin, 1947-',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '116',
				'name' => 'Waterhouse, Keith',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '115',
				'name' => 'Wan, Poe Yu-ze',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '114',
				'name' => 'Walters, Minette',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '113',
				'name' => 'Walker, Gordon P.',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '112',
				'name' => 'Wales. Welsh Government, ',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '111',
				'name' => 'Wai, Ken',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '110',
				'name' => 'Trail, Elizabeth',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '109',
				'name' => 'Tostevin, Helena',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '108',
				'name' => 'Tesar, George',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '107',
				'name' => 'Tanner, Marie, 1937-',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '106',
				'name' => 'Stadler, Christian',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '105',
				'name' => 'Sockett, Hugh',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '104',
				'name' => 'Smith, Moyra',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '103',
				'name' => 'Smith, Helen, 1977-',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '102',
				'name' => 'Smith, Emma (Emma Josephine)',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '101',
				'name' => 'Sinclair, Marta',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '100',
				'name' => 'Sinclair, John, 1944-',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '99',
				'name' => 'Sinclair, John, 1944-',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '98',
				'name' => 'Shale, Suzanne',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '97',
				'name' => 'Saunders, Peter, 1948-',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '96',
				'name' => 'Sanders, Erin Ladd',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '95',
				'name' => 'Sallis, James, 1944-',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '94',
				'name' => 'Sallis, James, 1944-',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '93',
				'name' => 'Sacks, David A.',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '92',
				'name' => 'Ross, Anamaria Iosif',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '91',
				'name' => 'Rohland, Eleonora',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '90',
				'name' => 'Rogers, Vanessa',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '89',
				'name' => 'Roberts, Nora',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '88',
				'name' => 'Rifkin, Jeremy',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '87',
				'name' => 'Randall, Alan, 1944-',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '86',
				'name' => 'Ramos, Pedro Pablo',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '85',
				'name' => 'Raffaelli, Tiziano, 1950-',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '84',
				'name' => 'Probert, Laura',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '83',
				'name' => 'Pre-school Learning Alliance, ',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '82',
				'name' => 'Post, Karen',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '81',
				'name' => 'Plenert, Gerhard Johannes',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '80',
				'name' => 'Pierse, Simon',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '79',
				'name' => 'Perelman, Marc',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '78',
				'name' => 'Parnell, Nicole E.',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '77',
				'name' => 'Pai, Hsiao-Hung',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '76',
				'name' => 'Olsen, Glenn W. (Glenn Warren), 1938-',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '75',
				'name' => 'Office for Public Management (London, England)',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '74',
				'name' => 'O Callaghan, Paul',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '73',
				'name' => 'Northrop, Jane Megan',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '72',
				'name' => 'Nichols, Aidan',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '71',
				'name' => 'Newton, Hannah',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '70',
				'name' => 'Nelson, Alondra',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '69',
				'name' => 'Murungi, John, 1943-',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '68',
				'name' => 'Murray, Kevin, 1928-',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '67',
				'name' => 'Moehringer, J. R., 1964-',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '66',
				'name' => 'Miller, Madeline',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '65',
				'name' => 'Meier, Daniel R.',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '64',
				'name' => 'Marchetti, Anne M., 1963-',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '63',
				'name' => 'Lowndes, Emma',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '62',
				'name' => 'Loeper, David B.',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '61',
				'name' => 'Lipsitz, George',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '60',
				'name' => 'Linton, Vivien',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '59',
				'name' => 'Lincoln, Bruce',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '58',
				'name' => 'Levy, Sidney M.',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '57',
				'name' => 'Lenk, Christian',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '56',
				'name' => 'Laine, Tarja',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '55',
				'name' => 'Kress, Diane',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '54',
				'name' => 'Kobilinsky, Lawrence F.',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '53',
				'name' => 'Kajfez, Darko',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '52',
				'name' => 'Jones, Aled, 1970-',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '51',
				'name' => 'Ingram, Barbara Lichner',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '50',
				'name' => 'Imeson, Anton',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '49',
				'name' => 'Hutchinson, Pat',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '48',
				'name' => 'Hogger, James',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '47',
				'name' => 'Hewitt, Geoff, 1953-',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '46',
				'name' => 'Herring, Richard, 1967-',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '45',
				'name' => 'Hebblethwaite, Benjamin',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '44',
				'name' => 'Hearst Books (Firm), ',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '43',
				'name' => 'Hayton, James C.',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '42',
				'name' => 'Hassan, Robert, 1959-',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '41',
				'name' => 'Hall, Peter, 1930-',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '40',
				'name' => 'Gurnah, Abdulrazak, 1948-',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '39',
				'name' => 'Gray, Linda, 1948-',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '38',
				'name' => 'Glannon, Walter',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '37',
				'name' => 'Gillespie, Robert M.',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '36',
				'name' => 'Gamble, Paul',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '35',
				'name' => 'Gallagher, Matt',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '34',
				'name' => 'Fraser-Sampson, Guy',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '33',
				'name' => 'Francis, Dick',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '32',
				'name' => 'Forsthoffer, W. E. (William E.)',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '31',
				'name' => 'Fitzek, Sebastian, 1971-',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '30',
				'name' => 'Espicom Business Intelligence, ',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '29',
				'name' => 'Escoffery, George',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '28',
				'name' => 'Edwards, Robert, 1950-',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '27',
				'name' => 'Drummond, Alex, 1964-',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '26',
				'name' => 'Drinnan, Arthur, 1925-',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '25',
				'name' => 'Diggs, Nancy Brown',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '24',
				'name' => 'De-Sammy, Christiana',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '23',
				'name' => 'DeGiovine, Vincent M.',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '22',
				'name' => 'Cross, E. N. (Edwin N.)',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '21',
				'name' => 'Crandall, Barbara F.',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '20',
				'name' => 'Connor, Jeff',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '19',
				'name' => 'Cipriano, Robert E., 1945-',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '18',
				'name' => 'Christie, Agatha, 1890-1976',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '17',
				'name' => 'Chesbrough, Henry William',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '16',
				'name' => 'Chan, Jacinta',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '15',
				'name' => 'Celmins, Martin',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '14',
				'name' => 'Campos, David',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '13',
				'name' => 'Bryce, Ian Kinloch, 1922-2011',
				'nationality' => 'Brazillian' 
		) );
		Author::create ( array (
				'id' => '12',
				'name' => 'Brown, Jane, 1938-',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '11',
				'name' => 'Bowker, Margaret',
				'nationality' => 'irish' 
		) );
		Author::create ( array (
				'id' => '10',
				'name' => 'Block, Stefan Merrill',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '9',
				'name' => 'Blenski, Christiane',
				'nationality' => 'American' 
		) );
		Author::create ( array (
				'id' => '8',
				'name' => 'Bjerg, Ole, 1974-',
				'nationality' => 'German' 
		) );
		Author::create ( array (
				'id' => '7',
				'name' => 'Ball, B. W. (Bryan W.)',
				'nationality' => 'English' 
		) );
		Author::create ( array (
				'id' => '6',
				'name' => 'Arnold, Glen',
				'nationality' => 'French' 
		) );
		Author::create ( array (
				'id' => '5',
				'name' => 'Arnett, Jeffrey Jensen',
				'nationality' => 'Brazillian' 
		) );
		
		Author::create ( array (
				'id' => '4',
				'name' => 'Woods, James C.',
				'nationality' => 'irish' 
		) );
		
		Author::create ( array (
				'id' => '3',
				'name' => 'Altman, Matthew C.',
				'nationality' => 'irish' 
		) );
		
		Author::create ( array (
				'id' => '2',
				'name' => 'Allen, James E. (James Elmore), 1935-',
				'nationality' => 'English' 
		) );
		
		Author::create ( array (
				'id' => '1',
				'name' => 'Ciara Foley',
				'nationality' => 'American' 
		) );
	}
}