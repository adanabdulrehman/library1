<?php

// class used to seed the user_roles table with data
class UserRoleSeeder extends Seeder {
	
	/**
	 * Run the user roles seeds.
	 *
	 * @return void
	 */
	public function run() {
		// DB::table ( 'users_roles' )->delete ();
		DB::table ( 'users_roles' )->insert ( array (
				array (
						'user_id' => '1',
						'role_id' => '0' 
				),
				array (
						'user_id' => '2',
						'role_id' => '1' 
				),
				array (
						'user_id' => '3',
						'role_id' => '2' 
				),
				array (
						'user_id' => '4',
						'role_id' => '0' 
				),
				array (
						'user_id' => '5',
						'role_id' => '0' 
				),
				array (
						'user_id' => '6',
						'role_id' => '0'
				),
				array (
						'user_id' => '7',
						'role_id' => '1'
				),
				array (
						'user_id' => '8',
						'role_id' => '2'
				)
		) );
	}
}