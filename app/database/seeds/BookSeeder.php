<?php

// class used to seed the books table with data
// this is used for testing purposes
class BookSeeder extends Seeder {
	
	/**
	 * Run the book seeds.
	 *
	 * @return void
	 */
	public function run() {
		// DB::table ( 'books' )->delete ();
		Book::create ( array (
				'id' => '1',
				'title' => 'Dress yourself like the Crabmeister',
				'isbn' => '18274827',
				'publication_date' => '14/03/1978',
				'author_id' => '1',
				'genre_id' => '4',
				'average_review_score' => '0.0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Maybe some detail about the book or a short description' 
		) );
		
		Book::create ( array (
				'id' => '145',
				'title' => 'Beyond Mars',
				'isbn' => '462790942',
				'publication_date' => '12/09/2010',
				'author_id' => '2',
				'genre_id' => '2',
				'average_review_score' => '0.0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Maybe some detail about the book or a short description' 
		) );
		
		Book::create ( array (
				'id' => '146',
				'title' => 'The revenge of the killer robots',
				'isbn' => '678686487',
				'publication_date' => '12/09/2012',
				'author_id' => '2',
				'genre_id' => '2',
				'average_review_score' => '0.0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Maybe some detail about the book or a short description' 
		) );
		
		Book::create ( array (
				'id' => '147',
				'title' => 'Feng shui your big toe to pleasure',
				'isbn' => '791676573',
				'publication_date' => '18/10/1948',
				'author_id' => '5',
				'genre_id' => '3',
				'average_review_score' => '0.0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Maybe some detail about the book or a short description' 
		) );
		
		Book::create ( array (
				'id' => '148',
				'title' => 'The collective mind: surfing to heaven',
				'isbn' => '889252422',
				'publication_date' => '23/07/1987',
				'author_id' => '5',
				'genre_id' => '3',
				'average_review_score' => '0.0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Maybe some detail about the book or a short description' 
		) );
		
		Book::create ( array (
				'id' => '149',
				'title' => 'How to wear a mankini inside out',
				'isbn' => '571285791',
				'publication_date' => '12/12/2012',
				'author_id' => '1',
				'genre_id' => '4',
				'average_review_score' => '0.0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Maybe some detail about the book or a short description' 
		) );
		
		Book::create ( array (
				'id' => '150',
				'title' => 'The Liar, The Bitch and the Wardrobe',
				'isbn' => '269279272',
				'publication_date' => '12/12/2011',
				'author_id' => '1',
				'genre_id' => '4',
				'average_review_score' => '0.0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Maybe some detail about the book or a short description' 
		) );
		
		Book::create ( array (
				'id' => '151',
				'title' => 'Twin Tollans : Chichen Itza, Tula, and the epiclassic to early postclassic Mesoamerican world',
				'isbn' => '047090772X',
				'publication_date' => '06/10/2013',
				'author_id' => '119',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :xxiii, 261 p.' 
		) );
		Book::create ( array (
				'id' => '144',
				'title' => 'Medicine and public health at the end of empire',
				'isbn' => '9781905375493',
				'publication_date' => '02/03/2014',
				'author_id' => '107',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Basilica di San Pietro in Vaticano--History' 
		) );
		Book::create ( array (
				'id' => '143',
				'title' => 'Understanding social work research',
				'isbn' => '9780749463595',
				'publication_date' => '27/05/2013',
				'author_id' => '106',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Corporations--Europe' 
		) );
		Book::create ( array (
				'id' => '142',
				'title' => 'Action research for business, nonprofit, & public administration : a tool for complex times',
				'isbn' => '9780415899970',
				'publication_date' => '12/12/2013',
				'author_id' => '105',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Education--Philosophy' 
		) );
		Book::create ( array (
				'id' => '141',
				'title' => 'The new law of torts',
				'isbn' => '9780195379631',
				'publication_date' => '30/05/2013',
				'author_id' => '104',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Phenotype' 
		) );
		Book::create ( array (
				'id' => '140',
				'title' => 'Selling sex in the Reich : prostitutes in German society, 1914-1945',
				'isbn' => '9780199651580',
				'publication_date' => '11/12/2013',
				'author_id' => '103',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Women in the book industries and trade--England--History--To 1500' 
		) );
		Book::create ( array (
				'id' => '139',
				'title' => 'There is no dog',
				'isbn' => '9780141192277',
				'publication_date' => '07/02/2014',
				'author_id' => '102',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Revenge--Drama' 
		) );
		Book::create ( array (
				'id' => '138',
				'title' => 'Twin Tollans : Chichen Itza, Tula, and the epiclassic to early postclassic Mesoamerican world',
				'isbn' => '047090772X',
				'publication_date' => '06/10/2013',
				'author_id' => '119',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :xxiii, 261 p.' 
		) );
		Book::create ( array (
				'id' => '137',
				'title' => 'Twin Tollans : Chichen Itza, Tula, and the epiclassic to early postclassic Mesoamerican world',
				'isbn' => '047090772X',
				'publication_date' => '06/10/2013',
				'author_id' => '119',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :xxiii, 261 p.' 
		) );
		Book::create ( array (
				'id' => '136',
				'title' => 'Collection development in the digital age',
				'isbn' => '9781849711517',
				'publication_date' => '18/06/2014',
				'author_id' => '118',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Resilience (Ecology)' 
		) );
		Book::create ( array (
				'id' => '135',
				'title' => 'Luchadoras',
				'isbn' => '9781857596694',
				'publication_date' => '21/09/2014',
				'author_id' => '117',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :National Trust (Great Britain)--History' 
		) );
		Book::create ( array (
				'id' => '134',
				'title' => 'Tagore and China',
				'isbn' => '9781907841002',
				'publication_date' => '26/03/2015',
				'author_id' => '116',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Luncheons' 
		) );
		Book::create ( array (
				'id' => '133',
				'title' => 'Lachaidh agus an oidhche iargalta',
				'isbn' => '9781409411529',
				'publication_date' => '20/01/2015',
				'author_id' => '115',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Social sciences--Philosophy' 
		) );
		Book::create ( array (
				'id' => '132',
				'title' => 'The shape of snakes',
				'isbn' => '9781447207955',
				'publication_date' => '06/05/2014',
				'author_id' => '114',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Alienation (Social psychology)--Fiction' 
		) );
		Book::create ( array (
				'id' => '131',
				'title' => 'Management : a developing country perspective',
				'isbn' => '9780415589734',
				'publication_date' => '16/08/2014',
				'author_id' => '113',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Environmental justice' 
		) );
		Book::create ( array (
				'id' => '130',
				'title' => 'Young people and NEETs in Europe : first findings : resume',
				'isbn' => '9780750468206',
				'publication_date' => '26/06/2013',
				'author_id' => '112',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Wales. Welsh Government--Buildings' 
		) );
		Book::create ( array (
				'id' => '129',
				'title' => 'Sketching masterclass',
				'isbn' => '9789812459299',
				'publication_date' => '23/06/2013',
				'author_id' => '111',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Wai, Ken' 
		) );
		Book::create ( array (
				'id' => '128',
				'title' => 'Feminist frontiers',
				'isbn' => '9781871828726',
				'publication_date' => '02/07/2014',
				'author_id' => '110',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Hornby, Constance, 1884-1972' 
		) );
		Book::create ( array (
				'id' => '127',
				'title' => 'Sports coaching : a reference guide for students, coaches and competitors',
				'isbn' => '9781444110678',
				'publication_date' => '19/06/2014',
				'author_id' => '109',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Portuguese language--Errors of usage' 
		) );
		Book::create ( array (
				'id' => '126',
				'title' => 'Arlidge, Eady & Smith on contempt',
				'isbn' => '9781848164956',
				'publication_date' => '01/12/2014',
				'author_id' => '108',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Manufacturing industries--Wisconsin--Management--Longitudinal studies' 
		) );
		Book::create ( array (
				'id' => '125',
				'title' => 'Medicine and public health at the end of empire',
				'isbn' => '9781905375493',
				'publication_date' => '02/03/2014',
				'author_id' => '107',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Basilica di San Pietro in Vaticano--History' 
		) );
		Book::create ( array (
				'id' => '124',
				'title' => 'Understanding social work research',
				'isbn' => '9780749463595',
				'publication_date' => '27/05/2013',
				'author_id' => '106',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Corporations--Europe' 
		) );
		Book::create ( array (
				'id' => '123',
				'title' => 'Action research for business, nonprofit, & public administration : a tool for complex times',
				'isbn' => '9780415899970',
				'publication_date' => '12/12/2013',
				'author_id' => '105',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Education--Philosophy' 
		) );
		Book::create ( array (
				'id' => '122',
				'title' => 'The new law of torts',
				'isbn' => '9780195379631',
				'publication_date' => '30/05/2013',
				'author_id' => '104',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Phenotype' 
		) );
		Book::create ( array (
				'id' => '121',
				'title' => 'Selling sex in the Reich : prostitutes in German society, 1914-1945',
				'isbn' => '9780199651580',
				'publication_date' => '11/12/2013',
				'author_id' => '103',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Women in the book industries and trade--England--History--To 1500' 
		) );
		Book::create ( array (
				'id' => '120',
				'title' => 'There is no dog',
				'isbn' => '9780141192277',
				'publication_date' => '07/02/2014',
				'author_id' => '102',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Revenge--Drama' 
		) );
		Book::create ( array (
				'id' => '119',
				'title' => 'Twin Tollans : Chichen Itza, Tula, and the epiclassic to early postclassic Mesoamerican world',
				'isbn' => '047090772X',
				'publication_date' => '06/10/2013',
				'author_id' => '119',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :xxiii, 261 p.' 
		) );
		Book::create ( array (
				'id' => '118',
				'title' => 'Collection development in the digital age',
				'isbn' => '9781849711517',
				'publication_date' => '18/06/2014',
				'author_id' => '118',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Resilience (Ecology)' 
		) );
		Book::create ( array (
				'id' => '117',
				'title' => 'Luchadoras',
				'isbn' => '9781857596694',
				'publication_date' => '21/09/2014',
				'author_id' => '117',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :National Trust (Great Britain)--History' 
		) );
		Book::create ( array (
				'id' => '116',
				'title' => 'Tagore and China',
				'isbn' => '9781907841002',
				'publication_date' => '26/03/2015',
				'author_id' => '116',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Luncheons' 
		) );
		Book::create ( array (
				'id' => '115',
				'title' => 'Lachaidh agus an oidhche iargalta',
				'isbn' => '9781409411529',
				'publication_date' => '20/01/2015',
				'author_id' => '115',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Social sciences--Philosophy' 
		) );
		Book::create ( array (
				'id' => '114',
				'title' => 'The shape of snakes',
				'isbn' => '9781447207955',
				'publication_date' => '06/05/2014',
				'author_id' => '114',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Alienation (Social psychology)--Fiction' 
		) );
		Book::create ( array (
				'id' => '113',
				'title' => 'Management : a developing country perspective',
				'isbn' => '9780415589734',
				'publication_date' => '16/08/2014',
				'author_id' => '113',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Environmental justice' 
		) );
		
		Book::create ( array (
				'id' => '112',
				'title' => 'Young people and NEETs in Europe : first findings : resume',
				'isbn' => '9780750468206',
				'publication_date' => '26/06/2013',
				'author_id' => '112',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Wales. Welsh Government--Buildings' 
		) );
		
		Book::create ( array (
				'id' => '111',
				'title' => 'Sketching masterclass',
				'isbn' => '9789812459299',
				'publication_date' => '23/06/2013',
				'author_id' => '111',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Wai, Ken' 
		) );
		
		Book::create ( array (
				'id' => '110',
				'title' => 'Feminist frontiers',
				'isbn' => '9781871828726',
				'publication_date' => '02/07/2014',
				'author_id' => '110',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Hornby, Constance, 1884-1972' 
		) );
		Book::create ( array (
				'id' => '109',
				'title' => 'Sports coaching : a reference guide for students, coaches and competitors',
				'isbn' => '9781444110678',
				'publication_date' => '19/06/2014',
				'author_id' => '109',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Portuguese language--Errors of usage' 
		) );
		Book::create ( array (
				'id' => '108',
				'title' => 'Arlidge, Eady & Smith on contempt',
				'isbn' => '9781848164956',
				'publication_date' => '01/12/2014',
				'author_id' => '108',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Manufacturing industries--Wisconsin--Management--Longitudinal studies' 
		) );
		Book::create ( array (
				'id' => '107',
				'title' => 'Medicine and public health at the end of empire',
				'isbn' => '9781905375493',
				'publication_date' => '02/03/2014',
				'author_id' => '107',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Basilica di San Pietro in Vaticano--History' 
		) );
		Book::create ( array (
				'id' => '106',
				'title' => 'Understanding social work research',
				'isbn' => '9780749463595',
				'publication_date' => '27/05/2013',
				'author_id' => '106',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Corporations--Europe' 
		) );
		Book::create ( array (
				'id' => '105',
				'title' => 'Action research for business, nonprofit, & public administration : a tool for complex times',
				'isbn' => '9780415899970',
				'publication_date' => '12/12/2013',
				'author_id' => '105',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Education--Philosophy' 
		) );
		Book::create ( array (
				'id' => '104',
				'title' => 'The new law of torts',
				'isbn' => '9780195379631',
				'publication_date' => '30/05/2013',
				'author_id' => '104',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Phenotype' 
		) );
		Book::create ( array (
				'id' => '103',
				'title' => 'Selling sex in the Reich : prostitutes in German society, 1914-1945',
				'isbn' => '9780199651580',
				'publication_date' => '11/12/2013',
				'author_id' => '103',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Women in the book industries and trade--England--History--To 1500' 
		) );
		Book::create ( array (
				'id' => '102',
				'title' => 'There is no dog',
				'isbn' => '9780141192277',
				'publication_date' => '07/02/2014',
				'author_id' => '102',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Revenge--Drama' 
		) );
		Book::create ( array (
				'id' => '101',
				'title' => 'The accidental salad',
				'isbn' => '9781848448889',
				'publication_date' => '19/11/1912',
				'author_id' => '101',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Decision making' 
		) );
		Book::create ( array (
				'id' => '100',
				'title' => 'Health security and governance : critical concepts in military, strategic, and security studies',
				'isbn' => '9780415668828',
				'publication_date' => '02/12/1983',
				'author_id' => '100',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Advertising--Social aspects' 
		) );
		Book::create ( array (
				'id' => '99',
				'title' => 'Anthropologists in the securityscape : ethics, practice, and professional identity',
				'isbn' => '9781904455929',
				'publication_date' => '07/02/1890',
				'author_id' => '99',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Flaviviruses' 
		) );
		Book::create ( array (
				'id' => '98',
				'title' => 'Oxidative stress in vertebrates and invertebrates : molecular aspects of cell signaling',
				'isbn' => '9781107006157',
				'publication_date' => '02/05/1963',
				'author_id' => '98',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Medical ethics--Great Britain' 
		) );
		Book::create ( array (
				'id' => '97',
				'title' => 'Clematis : an essential guide',
				'isbn' => '9781847428387',
				'publication_date' => '23/08/1850',
				'author_id' => '97',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Poverty--Australia' 
		) );
		Book::create ( array (
				'id' => '96',
				'title' => 'Top Gear portfolio : the worlds greatest cars',
				'isbn' => '9781741965674',
				'publication_date' => '11/11/1981',
				'author_id' => '96',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Pacifists--Biography' 
		) );
		Book::create ( array (
				'id' => '95',
				'title' => 'The curse box',
				'isbn' => '9781842437407',
				'publication_date' => '02/11/1920',
				'author_id' => '95',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Murder for hire--Fiction' 
		) );
		Book::create ( array (
				'id' => '94',
				'title' => 'Dont look back',
				'isbn' => '9781846272301',
				'publication_date' => '26/07/1839',
				'author_id' => '94',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Explorers--Fiction' 
		) );
		Book::create ( array (
				'id' => '93',
				'title' => 'Remembering the AIDS quilt',
				'isbn' => '9781580404372',
				'publication_date' => '08/07/1920',
				'author_id' => '93',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :\Diabetes and Pregnancyis a comprehensive guide for women with Type 1' 
		) );
		Book::create ( array (
				'id' => '92',
				'title' => 'Pensions law handbook',
				'isbn' => '9781845208011',
				'publication_date' => '17/04/1890',
				'author_id' => '92',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Medical anthropology' 
		) );
		Book::create ( array (
				'id' => '91',
				'title' => 'The life and times of Charles Henry Mackintosh : a biography, 1820-1896',
				'isbn' => '9781905472130',
				'publication_date' => '16/03/1966',
				'author_id' => '91',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Swiss Re (Firm)--History' 
		) );
		Book::create ( array (
				'id' => '90',
				'title' => 'Felt button bead : more than 35 creative fabric-crafting projects for kids aged 3-10',
				'isbn' => '9781849052221',
				'publication_date' => '25/11/1918',
				'author_id' => '90',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Emotions in children' 
		) );
		Book::create ( array (
				'id' => '89',
				'title' => 'Lethal rider',
				'isbn' => '749955414',
				'publication_date' => '24/03/1840',
				'author_id' => '89',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Boonsboro (Md.)--Fiction' 
		) );
		Book::create ( array (
				'id' => '88',
				'title' => 'Cable supported bridges : concept and design',
				'isbn' => '9780230115217',
				'publication_date' => '17/03/1976',
				'author_id' => '88',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Energy development' 
		) );
		Book::create ( array (
				'id' => '87',
				'title' => 'Arnhem',
				'isbn' => '9780521766159',
				'publication_date' => '09/08/1953',
				'author_id' => '87',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Risk' 
		) );
		Book::create ( array (
				'id' => '86',
				'title' => 'Patterns, prevention, and geometry of crime',
				'isbn' => '9780230320161',
				'publication_date' => '06/06/1858',
				'author_id' => '86',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Industrial organization' 
		) );
		Book::create ( array (
				'id' => '85',
				'title' => 'Air war Afghanistan : US and NATO air operations from 2001',
				'isbn' => '9781847205124',
				'publication_date' => '10/09/1906',
				'author_id' => '85',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Marshall, Alfred, 1842-1924--Influence' 
		) );
		Book::create ( array (
				'id' => '84',
				'title' => 'The green meridian : a walk across France',
				'isbn' => '9780955867415',
				'publication_date' => '07/11/1926',
				'author_id' => '84',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :World War, 1914-1918--Women--England--Thanet' 
		) );
		Book::create ( array (
				'id' => '83',
				'title' => 'Plants & society',
				'isbn' => '9781873743713',
				'publication_date' => '22/10/1867',
				'author_id' => '83',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Education, Preschool--Employees--Recruiting--Great Britain' 
		) );
		Book::create ( array (
				'id' => '82',
				'title' => 'Law, business and society',
				'isbn' => '9780071775281',
				'publication_date' => '23/10/1932',
				'author_id' => '82',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Brand name products' 
		) );
		Book::create ( array (
				'id' => '81',
				'title' => 'Perfect phrases for healthcare professionals : hundreds of ready-to-use phrases',
				'isbn' => '9780071767187',
				'publication_date' => '13/09/1915',
				'author_id' => '81',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Continuous improvement process' 
		) );
		Book::create ( array (
				'id' => '80',
				'title' => 'Seven days. Friday--Sunday',
				'isbn' => '9781409420545',
				'publication_date' => '30/01/1935',
				'author_id' => '80',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Art, Australian--England--London--20th century' 
		) );
		Book::create ( array (
				'id' => '79',
				'title' => 'Genes, cells and brains : biosciences Promethean promises',
				'isbn' => '9781844678594',
				'publication_date' => '11/12/1932',
				'author_id' => '79',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Sports--Social aspects' 
		) );
		Book::create ( array (
				'id' => '78',
				'title' => 'Psychology of trust',
				'isbn' => '9781612098272',
				'publication_date' => '11/04/1928',
				'author_id' => '78',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Radiation--Toxicology' 
		) );
		Book::create ( array (
				'id' => '77',
				'title' => 'Anholts artists activity book',
				'isbn' => '9781844678860',
				'publication_date' => '12/09/2011',
				'author_id' => '77',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Migrant labor--China--Social conditions--21st century' 
		) );
		Book::create ( array (
				'id' => '76',
				'title' => 'Inspiration & interpretation : a theological introduction to Sacred Scripture',
				'isbn' => '813217407',
				'publication_date' => '04/05/1896',
				'author_id' => '76',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Introduction -- Three premises -- Modernism -- The music of the spheres -- The loss of transcendence -- Alternatives.' 
		) );
		Book::create ( array (
				'id' => '75',
				'title' => 'The tax implications of charity trading',
				'isbn' => '1898531838',
				'publication_date' => '22/10/1860',
				'author_id' => '75',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Employee ownership--Great Britain' 
		) );
		Book::create ( array (
				'id' => '74',
				'title' => 'Murder city : Ciudad Ju?rez and the global economys new killing fields',
				'isbn' => '9780813218625',
				'publication_date' => '17/07/2010',
				'author_id' => '74',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Catholic Church--Doctrines' 
		) );
		Book::create ( array (
				'id' => '73',
				'title' => 'International relations, meaning and mimesis',
				'isbn' => '9780415521390',
				'publication_date' => '28/11/1999',
				'author_id' => '73',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Surgery, Plastic--Social aspects' 
		) );
		Book::create ( array (
				'id' => '72',
				'title' => 'Microbiology : with diseases by body system',
				'isbn' => '9780232528589',
				'publication_date' => '26/04/1860',
				'author_id' => '72',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Balthasar, Hans Urs von, 1905-1988. Herrlichkeit' 
		) );
		Book::create ( array (
				'id' => '71',
				'title' => 'Javier Mar?ases debt to translation : Sterne, Browne, Nabokov',
				'isbn' => '9780199650491',
				'publication_date' => '16/12/1849',
				'author_id' => '71',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Children--Health and hygiene--England--History--17th century' 
		) );
		Book::create ( array (
				'id' => '70',
				'title' => 'Kitten',
				'isbn' => '9780816676484',
				'publication_date' => '17/05/1910',
				'author_id' => '70',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Black Panther Party--Social aspects' 
		) );
		Book::create ( array (
				'id' => '69',
				'title' => 'Cherchez la femme : women and values in the Francophone world',
				'isbn' => '9781443829274',
				'publication_date' => '10/08/1862',
				'author_id' => '69',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Music--Africa--History and criticism' 
		) );
		Book::create ( array (
				'id' => '68',
				'title' => 'Arizona : a celebration of the Grand Canyon State',
				'isbn' => '9780749463960',
				'publication_date' => '14/07/1941',
				'author_id' => '68',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Leadership' 
		) );
		Book::create ( array (
				'id' => '67',
				'title' => 'Goodnight sweetheart',
				'isbn' => '034082882X',
				'publication_date' => '09/12/1957',
				'author_id' => '67',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Moehringer, J. R., 1964-' 
		) );
		Book::create ( array (
				'id' => '66',
				'title' => 'Dreams of Joy',
				'isbn' => '9781408821985',
				'publication_date' => '27/11/1929',
				'author_id' => '66',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Achilles (Greek mythology)--Fiction' 
		) );
		Book::create ( array (
				'id' => '65',
				'title' => 'The environmental vision of Thomas Merton',
				'isbn' => '080775238X',
				'publication_date' => '04/03/1906',
				'author_id' => '65',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Includes bibliographical references (p. 145-148) and index.' 
		) );
		Book::create ( array (
				'id' => '64',
				'title' => 'Its a jungle out there! : 52 nature adventures for city kids',
				'isbn' => '9780470917404',
				'publication_date' => '22/08/2006',
				'author_id' => '64',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :\High-level guidance for implementing enterprise risk management in any organization. A Practical Guide to Risk Management shows organizations how to implement an effective ERM solution' 
		) );
		Book::create ( array (
				'id' => '63',
				'title' => 'War and state building in the Middle East',
				'isbn' => '9781936320288',
				'publication_date' => '20/07/1845',
				'author_id' => '63',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Belloc, Bessie Rayner, 1829-1925' 
		) );
		Book::create ( array (
				'id' => '62',
				'title' => 'Stop the investing rip-off : how to avoid being a victim and make more money',
				'isbn' => '9781118133040',
				'publication_date' => '07/02/1901',
				'author_id' => '62',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Pensions--United States' 
		) );
		Book::create ( array (
				'id' => '61',
				'title' => 'Kinghorn Primary School and Nursery Class, Fife Council, 9 March 2010 : a report',
				'isbn' => '816666784',
				'publication_date' => '11/06/1867',
				'author_id' => '61',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Central Avenue breakdown -- Double crossing blues -- Willie and the hand jive -- Listen to the lambs -- All night long -- Play Misty for me -- The Watts breakaway -- Midnight at the Barrelhouse.' 
		) );
		Book::create ( array (
				'id' => '60',
				'title' => 'Faces & places of Kilrea',
				'isbn' => '9780857390523',
				'publication_date' => '02/10/1848',
				'author_id' => '60',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Teenagers writings, English' 
		) );
		Book::create ( array (
				'id' => '59',
				'title' => 'Colorful realm : Japanese bird-and-flower paintings by Ito Jakuchu',
				'isbn' => '226481875',
				'publication_date' => '23/06/1845',
				'author_id' => '59',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Includes bibliographical references and index.' 
		) );
		Book::create ( array (
				'id' => '58',
				'title' => 'The trading book : a complete solution to mastering technical systems and trading psychology',
				'isbn' => '71753109',
				'publication_date' => '19/06/1886',
				'author_id' => '58',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :xiv, 478 p.' 
		) );
		Book::create ( array (
				'id' => '57',
				'title' => 'Alaska for dummies',
				'isbn' => '9780199587551',
				'publication_date' => '29/01/1946',
				'author_id' => '57',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Tissue engineering' 
		) );
		Book::create ( array (
				'id' => '56',
				'title' => 'Ethnicity, race and education : an introduction',
				'isbn' => '9781441168153',
				'publication_date' => '16/04/1992',
				'author_id' => '56',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Emotions in motion pictures' 
		) );
		Book::create ( array (
				'id' => '55',
				'title' => 'Owl vs mouse',
				'isbn' => '738214450',
				'publication_date' => '10/04/2007',
				'author_id' => '55',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Includes index.' 
		) );
		Book::create ( array (
				'id' => '54',
				'title' => 'Julius Caesar : the curse of the gods',
				'isbn' => '9780471739548',
				'publication_date' => '14/08/1855',
				'author_id' => '54',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Chemistry, Forensic' 
		) );
		Book::create ( array (
				'id' => '53',
				'title' => 'Managing allegations and serious concerns about foster carers practice or standards of care : information booklet for practitioners',
				'isbn' => '9781608071616',
				'publication_date' => '27/03/1895',
				'author_id' => '53',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :MATLAB' 
		) );
		Book::create ( array (
				'id' => '52',
				'title' => 'A serious glance at chemistry : basic notions explained',
				'isbn' => '9781848091207',
				'publication_date' => '07/03/1930',
				'author_id' => '52',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Carols, English' 
		) );
		Book::create ( array (
				'id' => '51',
				'title' => 'The fast forward MBA in project management',
				'isbn' => '1118038223',
				'publication_date' => '10/04/1883',
				'author_id' => '51',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :xvii, 505 p.' 
		) );
		Book::create ( array (
				'id' => '50',
				'title' => 'Radio : essays in bad reception',
				'isbn' => '9780470714485',
				'publication_date' => '19/12/1879',
				'author_id' => '50',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Desertification' 
		) );
		Book::create ( array (
				'id' => '49',
				'title' => 'Stephen Roach on the next Asia : opportunities and challenges for a new globalization',
				'isbn' => '9780273735427',
				'publication_date' => '14/05/1997',
				'author_id' => '49',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Selling' 
		) );
		Book::create ( array (
				'id' => '48',
				'title' => 'Untold stories : Beachy Head',
				'isbn' => '9781848970960',
				'publication_date' => '08/03/1951',
				'author_id' => '48',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Teenagers--Fiction' 
		) );
		Book::create ( array (
				'id' => '47',
				'title' => 'Preqin global private equity review',
				'isbn' => '9781844157853',
				'publication_date' => '01/12/1866',
				'author_id' => '47',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Great Britain. Royal Navy--History--World War, 1939-1945' 
		) );
		Book::create ( array (
				'id' => '46',
				'title' => 'Top tips for weaning',
				'isbn' => '9780091932091',
				'publication_date' => '11/07/1916',
				'author_id' => '46',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Herring, Richard, 1967-' 
		) );
		Book::create ( array (
				'id' => '45',
				'title' => 'Education and HIV\/AIDS : education as a humanitarian response',
				'isbn' => '1439906025',
				'publication_date' => '28/09/1970',
				'author_id' => '45',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Includes bibliographical references ( p. [345]-351), filmography, discography (p.352) and index.' 
		) );
		Book::create ( array (
				'id' => '44',
				'title' => 'London',
				'isbn' => '158816666X',
				'publication_date' => '20/06/2003',
				'author_id' => '44',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Teenage girls--United States--Miscellanea--Juvenile literature' 
		) );
		Book::create ( array (
				'id' => '43',
				'title' => 'Mathematics teachers at work : connecting curriculum materials and classroom instruction',
				'isbn' => '9780415893701',
				'publication_date' => '22/10/1961',
				'author_id' => '43',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Personnel management--Case studies' 
		) );
		Book::create ( array (
				'id' => '42',
				'title' => 'Housing aspirations for a new generation : perspectives from white and South Asian British women',
				'isbn' => '9781412843065',
				'publication_date' => '16/08/1840',
				'author_id' => '42',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Time--Sociological aspects' 
		) );
		Book::create ( array (
				'id' => '41',
				'title' => 'Prediction markets : the end of the regulatory state?',
				'isbn' => '9781840029192',
				'publication_date' => '21/06/1964',
				'author_id' => '41',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Shakespeare, William, 1564-1616--Dramatic production' 
		) );
		Book::create ( array (
				'id' => '40',
				'title' => 'You',
				'isbn' => '9781408821855',
				'publication_date' => '02/11/1867',
				'author_id' => '40',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Families, Black--England--Fiction' 
		) );
		Book::create ( array (
				'id' => '39',
				'title' => 'The history of modern U.S. corporate governance',
				'isbn' => '9781847737069',
				'publication_date' => '08/04/1875',
				'author_id' => '39',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Fruit-culture' 
		) );
		Book::create ( array (
				'id' => '38',
				'title' => 'The development of grammar : language acquisition and diachronic change : in honour of J?rgen M. Meisel',
				'isbn' => '9780199734092',
				'publication_date' => '17/07/1856',
				'author_id' => '38',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Neurosciences--Moral and ethical aspects' 
		) );
		Book::create ( array (
				'id' => '37',
				'title' => 'Michael Collins and the Anglo-Irish War : Britains counterinsurgency failure',
				'isbn' => '9781591143215',
				'publication_date' => '01/07/1945',
				'author_id' => '37',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :United States. Military Assistance Command, Vietnam. Studies and Observations Group--History' 
		) );
		Book::create ( array (
				'id' => '36',
				'title' => 'Trust and conflict : representation, culture and dialogue',
				'isbn' => '9780415591256',
				'publication_date' => '03/02/1843',
				'author_id' => '36',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Physical education and training' 
		) );
		Book::create ( array (
				'id' => '35',
				'title' => 'Special needs language and literacy assessment handbook : for primary and secondary schools',
				'isbn' => '9780593067109',
				'publication_date' => '06/02/1996',
				'author_id' => '35',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Gallagher, Matt' 
		) );
		Book::create ( array (
				'id' => '34',
				'title' => 'Web design : introductory',
				'isbn' => '9780749463878',
				'publication_date' => '17/09/1913',
				'author_id' => '34',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Finance' 
		) );
		Book::create ( array (
				'id' => '33',
				'title' => 'The ... guide to Brazil',
				'isbn' => '330266624',
				'publication_date' => '10/06/1914',
				'author_id' => '33',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Jockeys--England--Fiction' 
		) );
		Book::create ( array (
				'id' => '32',
				'title' => 'Summer fling',
				'isbn' => '9780080966762',
				'publication_date' => '22/09/1868',
				'author_id' => '32',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Turbomachines' 
		) );
		Book::create ( array (
				'id' => '31',
				'title' => 'A critical woman : Barbara Wootton, social science and public policy in the twentieth century',
				'isbn' => '9781848876958',
				'publication_date' => '12/11/2000',
				'author_id' => '31',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Memory--Fiction' 
		) );
		Book::create ( array (
				'id' => '30',
				'title' => 'Eva Rothschild : Hot touch',
				'isbn' => '9781858224244',
				'publication_date' => '03/11/2010',
				'author_id' => '30',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Medical instruments and apparatus industry--Statistics' 
		) );
		Book::create ( array (
				'id' => '29',
				'title' => 'Legal issues in professional basketball',
				'isbn' => '9781844269419',
				'publication_date' => '10/03/1947',
				'author_id' => '29',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :God--Biblical teaching' 
		) );
		Book::create ( array (
				'id' => '28',
				'title' => 'The devi of speech : the goddess in Kundalini yoga',
				'isbn' => '9780952590972',
				'publication_date' => '16/08/1898',
				'author_id' => '28',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Edwards, Robert, 1950---Diaries' 
		) );
		Book::create ( array (
				'id' => '27',
				'title' => 'Saving for retirement : intention, con, and behavior',
				'isbn' => '9780956557995',
				'publication_date' => '19/04/2002',
				'author_id' => '27',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Transgender people--Identity' 
		) );
		Book::create ( array (
				'id' => '26',
				'title' => 'MCTS self-paced training kit (Exam 70-642) : configuring Windows Server 2008 network infrastructure',
				'isbn' => '9780956749901',
				'publication_date' => '21/07/1895',
				'author_id' => '26',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Drinnan, Arthur, 1925-' 
		) );
		Book::create ( array (
				'id' => '25',
				'title' => 'Homeless in Las Vegas : stories from the street',
				'isbn' => '870139975',
				'publication_date' => '26/07/1963',
				'author_id' => '25',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Getting here -- Working here -- Strangers in a strange land -- Living here -- Community reaction -- Law and disorder -- Looking back -- Mexico -- Other countries -- The good news.' 
		) );
		Book::create ( array (
				'id' => '24',
				'title' => 'Record of Cambodias land & customs',
				'isbn' => '9780956758200',
				'publication_date' => '28/05/1938',
				'author_id' => '24',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Success' 
		) );
		Book::create ( array (
				'id' => '23',
				'title' => 'The book of the Standard Motor Company',
				'isbn' => '9781612096537',
				'publication_date' => '07/10/1988',
				'author_id' => '23',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Dogs' 
		) );
		Book::create ( array (
				'id' => '22',
				'title' => 'Soldier no more : an undercover journalist in Zimbabwe 2000-2010',
				'isbn' => '9781853072468',
				'publication_date' => '04/05/1875',
				'author_id' => '22',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Mackintosh, Charles Henry, 1820-1896' 
		) );
		Book::create ( array (
				'id' => '21',
				'title' => 'Amazing grace',
				'isbn' => '9781441148711',
				'publication_date' => '10/11/1951',
				'author_id' => '21',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Women and religion' 
		) );
		Book::create ( array (
				'id' => '20',
				'title' => 'Ireland, Sweden and the Great European Migration : 1815-1914',
				'isbn' => '9781845961718',
				'publication_date' => '09/04/1841',
				'author_id' => '20',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Roche, Stephen, 1959-' 
		) );
		Book::create ( array (
				'id' => '19',
				'title' => 'Voice and phenomenon : introduction to the problem of the sign in Husserls phenomenology',
				'isbn' => '9780470903018',
				'publication_date' => '16/01/1985',
				'author_id' => '19',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :ix, 212 p.' 
		) );
		Book::create ( array (
				'id' => '18',
				'title' => 'They do it with mirrors',
				'isbn' => '7451660',
				'publication_date' => '31/07/1850',
				'author_id' => '18',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Baghdad (Iraq)--Fiction' 
		) );
		Book::create ( array (
				'id' => '17',
				'title' => 'The critical development studies handbook : tools for change',
				'isbn' => '9780470905746',
				'publication_date' => '03/05/1991',
				'author_id' => '17',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Customer services' 
		) );
		Book::create ( array (
				'id' => '16',
				'title' => 'Jacques Derrida : law as absolute hospitality',
				'isbn' => '273751336',
				'publication_date' => '09/10/1885',
				'author_id' => '16',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Technical analysis (Investment analysis)' 
		) );
		Book::create ( array (
				'id' => '15',
				'title' => 'Good News Bible. New Testament',
				'isbn' => '1898141134',
				'publication_date' => '21/07/1947',
				'author_id' => '15',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Green, Peter, guitarist' 
		) );
		Book::create ( array (
				'id' => '14',
				'title' => 'Partnerships for new teacher learning : a guide for universities and school districts',
				'isbn' => '807751790',
				'publication_date' => '05/08/1969',
				'author_id' => '14',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Overview -- The significance of health and wellness -' 
		) );
		Book::create ( array (
				'id' => '13',
				'title' => 'An open letter to the Party',
				'isbn' => '1841040436',
				'publication_date' => '20/09/1841',
				'author_id' => '13',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Bryce, Ian Kinloch, 1922-2011' 
		) );
		Book::create ( array (
				'id' => '12',
				'title' => 'Ribble',
				'isbn' => '9780701182120',
				'publication_date' => '09/06/1924',
				'author_id' => '12',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Brown, Lancelot, 1716-1783' 
		) );
		Book::create ( array (
				'id' => '11',
				'title' => 'Essays on Shakespeare',
				'isbn' => '521236398',
				'publication_date' => '07/09/1845',
				'author_id' => '11',
				'genre_id' => '3',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Church of England. Diocese of Lincoln--History--16th century' 
		) );
		Book::create ( array (
				'id' => '10',
				'title' => 'Cantata-140',
				'isbn' => '9780571269594',
				'publication_date' => '04/03/1989',
				'author_id' => '10',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Marriage--Fiction' 
		) );
		Book::create ( array (
				'id' => '9',
				'title' => 'In the service of 11:11 : a celestial\/mortal alliance',
				'isbn' => '9781845843328',
				'publication_date' => '02/09/1876',
				'author_id' => '9',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Games for dogs' 
		) );
		Book::create ( array (
				'id' => '8',
				'title' => 'Katie Lowery : in the round',
				'isbn' => '9780472071630',
				'publication_date' => '31/05/1984',
				'author_id' => '8',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Poker' 
		) );
		Book::create ( array (
				'id' => '7',
				'title' => 'Working for capitalism',
				'isbn' => '227678443',
				'publication_date' => '20/12/1871',
				'author_id' => '7',
				'genre_id' => '4',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :General Conference of Seventh-Day Adventists--Doctrines' 
		) );
		Book::create ( array (
				'id' => '6',
				'title' => 'HTML5 developers cookbook',
				'isbn' => '9780273730354',
				'publication_date' => '12/04/1994',
				'author_id' => '6',
				'genre_id' => '2',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Financial institutions--books' 
		) );
		Book::create ( array (
				'id' => '5',
				'title' => 'Personality theories workbook',
				'isbn' => '9780199757176',
				'publication_date' => '20/08/1999',
				'author_id' => '5',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Young adults--Psychology' 
		) );
		Book::create ( array (
				'id' => '4',
				'title' => 'The Solicitor General and the United States Supreme Court : executive branch influence and judicial decisions',
				'isbn' => '9781107013827',
				'publication_date' => '18/05/1934',
				'author_id' => '4',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Justice, Administration of--Asia' 
		) );
		Book::create ( array (
				'id' => '3',
				'title' => 'Godzilla. Kingdom of monsters',
				'isbn' => '9780470657669',
				'publication_date' => '24/05/1862',
				'author_id' => '3',
				'genre_id' => '1',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :Kant, Immanuel, 1724-1804' 
		) );
		Book::create ( array (
				'id' => '2',
				'title' => 'Jonas & Kovners health care delivery in the United States',
				'isbn' => '826107060',
				'publication_date' => '21/05/1955',
				'author_id' => '2',
				'genre_id' => '5',
				'average_review_score' => '0',
				'reserved' => '0',
				'available' => '1',
				'status' => '',
				'comment' => 'Publisher: : comment :xix, 156 p.' 
		) );
	}
}