<?php

// review model
class Review extends Eloquent {
	
	// An review will belong to a book that has been loaned
	public function book() {
		return $this->belongsTo ('Book');
	}
	
	// An review will belong to a book that has been loaned
	public function user() {
		return $this->belongsTo ('User');
	}
}