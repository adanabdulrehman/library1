<?php

// loan model
class Loan extends Eloquent {
	
	public function reviews() {
		return $this->hasMany ( 'Review' );
	}
	
	public function book() {
		return $this->belongsTo ( 'Book' );
	}
	
	public function user() {
		return $this->belongsTo ( 'User' );
	}
	
	// define a query scope for members pending approval
	public function scopeDue($query)
	{
		return $query->where('status', '!=', 'returned');
	}
}