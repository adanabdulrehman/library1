<?php

// user model
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

// this laravel user class is provided by default and used for authentication
class User extends Eloquent implements UserInterface, RemindableInterface {

	// Set the Guarded Attributes On The Model to protect from mass assignment
    protected $guarded = array('id', 'email');
		
	//  create our validation rules that we'll validate the form data against.
	public static $rules = array(
			'firstname'=>'required|alpha|between:2,50',
			'secondname'=>'required|alpha|min:2,50',
			'city'=>'required|alpha_num|between:2,50',
			'address'=>'required|between:2,253',
			'phone'=>'alpha_num|between:6,50',
			'username'=>'required|alpha|min:2',
			'email'=>'required|email|unique:users',
			'password'=>'required|alpha_num|between:6,12|confirmed',
			'password_confirmation'=>'required|alpha_num|between:6,12'
	);
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken()
	{
		return $this->remember_token;
	}
	
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}
	
	public function getRememberTokenName()
	{
		return 'remember_token';
	}
	
	/**
	 * Get the roles a user has
	 */
	public function roles()
	{
		return $this->belongsToMany('Role', 'users_roles');
	}
	
	/**
	 * Find out if User is a library user, based on if has any roles
	 *
	 * @return boolean
	 */
	public function isLibraryUser()
	{
		$roles = $this->roles->toArray();
		return !empty($roles);
	}
	
	/**
	 * Find out if user has a specific role
	 *
	 * $return boolean
	 */
	public function hasRole($check)
	{
		return in_array($check, array_fetch($this->roles->toArray(), 'name'));
	}
	
	/**
	 * Get key in array with corresponding value
	 *
	 * @return int
	 */
	private function getIdInArray($array, $term)
	{
		foreach ($array as $key => $value) {
			if ($value == $term) {
				return $key;
			}
		}
	
		throw new UnexpectedValueException;
	}
	
	/**
	 * Add roles to user to make them a library User
	 * Passing the title will determine roles
	 */
	public function makeLibraryUser($title)
	{
		$assigned_roles = array();
	
		$roles = array_fetch(Role::all()->toArray(), 'name');
	
		switch ($title) {
			case 'super_administrator':
				$assigned_roles[] = $this->getIdInArray($roles, 'super_administrator');
			case 'administrator':
				$assigned_roles[] = $this->getIdInArray($roles, 'administrator');
			case 'librarian':
				$assigned_roles[] = $this->getIdInArray($roles, 'librarian');
			case 'member':
				$assigned_roles[] = $this->getIdInArray($roles, 'member');
				break;
			default:
				throw new \Exception("The employee status entered does not exist");
		}
	
		$this->roles()->attach($assigned_roles);
	}
	
	// define a query scope for members that have outstanding fines
	public function scopeWithOutstandingFines($query)
	{
		return $query->where('outstanding_fine', '>', '0.00');
	}
	
	// define a query scope for members that have no outstanding fines
	public function scopeWithoutOutstandingFines($query)
	{
		return $query->where('outstanding_fine', '=', '0.00');
	}
	
	// define a query scope for members that have been approved
	public function scopeApproved($query)
	{
		return $query->where('status', '=', 'complete');
	}
	
	// define a query scope for members pending approval
	public function scopePendingApproval($query)
	{
		return $query->where('status', '=', 'pending');
	}
	
	public function scopeBookLoans($query) {
		return $this->loans()->due();
	}
	
	// A user will have many book loans
	public function loans() {
		return $this->hasMany ( 'Loan' );
	}
	
	// A user will have many book loans
	public function reviews() {
		return $this->hasMany ( 'Review' );
	}
}