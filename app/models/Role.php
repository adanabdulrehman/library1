<?php

// role model
class Role extends Eloquent
{
    /**
     * Set timestamps off
     */
    public $timestamps = false;
 
    /**
     * Get users with a certain role
     * This is a many to many relationship.
     * Example of a relationship is a user with many roles, 
     * where the roles are also shared by other users. 
     * For example, many users may have the role of "Admin". 
     */
    public function users()
    {
        return $this->belongsToMany('User', 'users_roles');
    }
}