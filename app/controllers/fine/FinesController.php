<?php

// controller responsible for handling the fines
class FinesController extends BaseController {
	
	function __construct() {
		
		// authenticate all actions based on a custom filter
		$this->beforeFilter ( 'librarian', array (
				'except' => array () 
		) );
		
		// protecting any post actions from csrf attacks
		$this->beforeFilter ( 'csrf', array (
				'on' => 'post' 
		) );
	}
	
	// post action to get a member 
	public function showMembersWithFines() {
		
		$membersWithFines = User::withOutstandingFines ()->orderBy ( 'firstname', 'secondname' )->paginate(10);
		return View::make ( 'report/membersWithFines' )->with ( 'users', $membersWithFines );
	}
	
	public function updateMembersWithFines() {
		$userId = Input::get ( 'user_id' );
	
		// checking which submit was clicked on
		if (Input::get ( 'payFine' )) {
			
			// update the fine amount for the user
			$user = User::find ( $userId );
			$user->outstanding_fine = 0.00;
			$user->update ();
				
			$membersWithFines = User::withOutstandingFines ()->orderBy ( 'firstname', 'secondname' )->paginate(10);
			
			$message = "fines updated";
			
			return View::make ( 'report/membersWithFines' )->with ( 'users', $membersWithFines )->with('message', $message);
		
		} else if (Input::get ( 'sendReminder' )) {
			
			// send an email but you need to set up the email server
			// since this was developed locally an email server
			// could not be configured. So commenting the code here and you need to
			// fill appropriate values and uncomment. Ideally the values should
			// be located in the database or properties file.
			// Mail::pretend ();
		/*	$data = Array();
			$data = "There is a large ammount of arrears on your account. Please make arrangements to pay fines";
			
			Mail::send ( 'emails.finesReminder', $data, function ($message) {
				// $message->to ( 'groupSixTempEmail@gmail.com', 'Persons name' )->subject ( 'Payment of arrears on Library books borrowed!' );	
				$message->to ( 'foo@example.com', 'John Smith' )->subject ( 'Payment of arrears on Library books borrowed!' );
			} );
		*/
			$membersWithFines = User::withOutstandingFines ()->orderBy ( 'firstname', 'secondname' )->paginate(10);
			$message = "Email sent";
			
			return View::make ( 'report/membersWithFines' )->with ( 'users', $membersWithFines )->with('message', $message);
		}
	}
}