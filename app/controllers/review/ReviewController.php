<?php

use Illuminate\Support\Facades\View;
class ReviewController extends \BaseController {

	// add a filter for authorization only allowing index and show resource
	function __construct() {
		// authenticate all actions except index and show
		$this->beforeFilter ( 'member', array ('except' => array ('show')) );
	
		// protecting any post actions from csrf attacks
		$this->beforeFilter('csrf', array('on'=>'post'));
	}
	// Display a listing of the reviewable books.
	// @return Response
	public function index() {
		
		if(Auth::check()) {
			$id = Auth::user()->id;
		}else{
			$id=0;
		}
		$loans = Loan::where('user_id', '=', $id)->groupBy('book_id')->get();
	
		return View::make('review.index')->with( 'loans', $loans);
	}

	// Show the form for creating a new review.
	// @return Response
	public function create($item_id) {

		$book = Book::find ( $item_id );
		$message = Session::get ( 'message', '' );
		
		$review = new Review;
		return View::make ('review.create')->with ( 'review', $review )->with ( 'book', $book );
	}

	// Store a newly created review in storage.
	// @return Response
	public function store() {

		$review=new Review;

		$inputs = Input::all ();
		$review->book_id =$inputs ['book_id'];
		$review->user_id = Auth::user()->id;
		$review->rating = $inputs ['rating'];
		$review->comment = $inputs ['comment'];
		$review->save();
		
		Loan::where('book_id', '=', $inputs ['book_id'])->where('user_id', '=', $review->user_id)->update(array('reviewed' => 1));

		return Redirect::route ('review.index');
	}

	// Display the reviews.
	// @param int $id
	// @return Response
	public function show($bookid) {

		$book = Book::find($bookid);
		
		// There is a bug with pagination in Laravel where
		// it paginates the data even within the controller
		// so we a another collection to work with locally
		$reviewsForAverage = $book->reviews;
		
		// paginating the reviews since we are anticipating a whole load
		$reviews = $book->reviews()->paginate(3);
		
		$message = Session::get('message', '');

		// extending the blade templete to register a new tag
		// for variables
		Blade::extend(function($value) {
			return preg_replace('/\{\?(.+)\?\}/', '<?php ${1} ?>', $value);
		});
		
		$totalScore = 0;
		$averageScore = 0;
		
		if(count($reviewsForAverage)) {
			
			foreach($reviewsForAverage as $review) {
				$totalScore = $totalScore + $review->rating;
			}
			
			$averageScore = $totalScore / count($reviewsForAverage);
		}
				
		return View::make ('review.show')->with ('reviews', $reviews)->with ('book', $book)->with('averageScore', $averageScore)->with('message', $message);
	}
}
