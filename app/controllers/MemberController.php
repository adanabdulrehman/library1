<?php

use Illuminate\Support\Facades\Redirect;
// This is the member controller
class MemberController extends BaseController {

	function __construct() {
	
		// authenticate all actions except index and show
		$this->beforeFilter ( 'librarian', array (
				'except' => array ('home')
		) );
		
		
		// protecting any post actions from csrf attacks
		$this->beforeFilter ( 'csrf', array (
				'on' => 'post'
		) );
	}
	
	// Default Home Controller
	public function home() {
		return Redirect::route ('default');
		//return View::make('member/home');
	}
	
	// @return Response
	public function index() {
		
		$members = User::where('status', '=', 'complete')->orderBy ( 'created_at' )->paginate(15);
		$numberOfMembers = 0;
		
		foreach ( $members as $member ) {
			if ($member->hasRole ( 'member' )) {
				$numberOfMembers ++;
			}
		}
		return View::make ( 'member.index' )->with ( 'users', $members )->with ( 'numberOfMembers', $numberOfMembers );
	}
}