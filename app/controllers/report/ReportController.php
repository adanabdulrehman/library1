<?php

// controller responsible for handling the generation
// of various administrator reports
class ReportController extends BaseController {
	
	function __construct() {
		
		// authenticate all actions based on a custom filter
		$this->beforeFilter ( 'Administrator', array (
				'except' => array () 
		) );
		
		// protecting any post actions from csrf attacks
		$this->beforeFilter ( 'csrf', array (
				'on' => 'post' 
		) );
	}
	
	public function showMostFined() {
		$finedUserData = User::withOutstandingFines ()->orderBy ( 'outstanding_fine' )->get ();
		return View::make ( 'report/mostFined' )->with ( 'finedUserData', $finedUserData )->with ( 'message', "Report completed" );
	}
}