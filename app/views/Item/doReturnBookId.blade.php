@extends('layouts.librarian.main')
	@section('header') 
		BOOK RETURN 
	@stop
	
	@section('leftMenu')
	@parent 
	@stop 
	
	@section('content')
	<section class="book_edit_form">
		<h2> Please provide book id of a book on loan below</h2><br/>	
	    {{Form::open(array('url' => '/doCheckinGetBook', 'method' => 'post')) }}
	    {{{ isset($message) ? $message : '' }}}
		    <span>&nbsp;</span>
		    <label><span>Book id :</span>{{Form::text('bookId')}}</label>
			<label><span>&nbsp;</span>{{Form::submit('Get Book')}}</label>

    	{{Form::close()}}
    	
    </section> 
	@stop