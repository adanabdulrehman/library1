@extends('layouts.librarian.main')
	@section('header') 
		BOOK LENDING 
	@stop
	
	@section('leftMenu')
	@parent 
	@stop 
	
	@section('content')

	<section class="book_edit_form">
		<h2> Please provide member id</h2><br/>	
	    {{Form::open(array('url' => '/doCheckoutGetMember', 'method' => 'post')) }}
		    {{{ isset($message) ? $message : '' }}}
		    <span>&nbsp;</span>
		    <label><span>Member id :</span>{{Form::text('memberId')}}</label>
			<label><span>&nbsp;</span>{{Form::submit('Get member')}}</label>

    	{{Form::close()}}
    	
    </section> 
	@stop