@extends('layouts.librarian.main')

@section('header')
	LIST OF UNAPPROVED MEMBERS
@stop
	
	
@section('leftMenu')
@parent
@stop
	
@section('content')
	<section class="booklist">
		<br>{{{ isset($message) ? $message : '' }}}
	@if($numberOfMembers > 0)
	<table> 
		<thead>
		    <tr>
		    <td>First name</td>
		    <td>Second name</td>
		    <td>Address</td>
		    <td>City</td>
		    <td>Phone No</td>
		        <td>Id</td>
				<td>Username</td>
				<td>Email</td>
				<td>Approve</td>
				<td>Decline</td>
				
			</tr>
		</thead>
 		
 		<tbody>
 		
		@foreach($users as $user)
			
				{{ Form::open(array('action'=>'approveMemberController@postUpdate')) }}
				<tr>
					<td>{{{$user->firstname}}}</a></td>
					<td>{{{$user->secondname}}}</a></td>
					<td>{{{$user->address}}}</a></td>
					<td>{{{$user->city}}}</a></td>
					<td>{{{$user->phone}}}</a></td>
				    <td>{{{$user->id}}}</td>
					<td>{{{$user->username}}}</a></td>
					<td>{{{$user->email}}}</td>
					<td><input type="submit" name="Approve" value="Approve"></td>
					<td><input type="submit" name="Decline" value="Decline"></td>
					<label><span></span>{{Form::hidden('user_id', $user->id)}}</label>
					
				{{ Form::close() }}
				</tr>
	
		@endforeach
		</tbody>
	</table>
	@else
	  <br>No members to approve
	@endif
	</section>
	<br/>
@stop