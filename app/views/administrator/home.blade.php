@extends('layouts.administrator.main')
	@section('header')
		ADMINISTRATOR PANEL
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
@section('content')
	<br> ADMINISTRATOR PANEL
	<br>Welcome to the city council book lending library administrator panel page.
	<br>To access functionality please choose a menu item on the left hand side.
	<br>
	<br>Any problems should be directed to admin@leabharlann.com
	<br>Alternatively, by phone at 012 345 678 Ext 9
	<br>
	<br>Thank you
	<br>Administration
	<br/>
@stop