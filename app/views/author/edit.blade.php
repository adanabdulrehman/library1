@extends('layouts.member.main')

	@section('header')
		Edit {{{$author->name}}}
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
	<section class="book_edit_form">
		{{Form::model($author, array('route' => array('author.update', $author->id), 'method' => 'put'))}}
		    <h1> {{{$author->name}}}  
		    <span>{{Form::label('id', $author->id)}}</span>
		    <span>&nbsp;</span>
		    </h1>
		    
		    <label><span>Name :</span>{{Form::text('name')}}</label>
		    <label><span>Nationality :</span>{{Form::text('nationality')}}</label>
			<label><span>&nbsp;</span>{{Form::submit('Update')}}</label>

    	{{Form::close()}}
    </section> 
	@stop
	