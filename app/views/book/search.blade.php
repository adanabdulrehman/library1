@extends('layouts.member.main')

	@section('header')
		BOOK SEARCH 
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
	<section class="book_edit_form">
	
	    {{Form::open(array('url' => '/bookSearch', 'method' => 'post')) }}
		    <span>&nbsp;</span>
		    </h1>
		    <label><span>Search for :</span>{{Form::text('searchTerm')}}</label>
		    <label><span>By :</span>{{Form::select('criteria', $criteria, 'title')}}</label>
			<label><span>&nbsp;</span>{{Form::submit('Search books')}}</label>

    	{{Form::close()}}
    </section> 
	@stop
	