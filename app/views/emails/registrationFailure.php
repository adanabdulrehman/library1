<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Reset</h2>

		<div>
			{{ URL::to('password/reset', array($token)) }}
		</div>
	</body>
</html>