@extends('layouts.member.main')

@section('header')
	LISTING ALL CATEGORIES
@stop
	
	
@section('leftMenu')
@parent
@stop
	
@section('content')
	@if(count($genres) < 1)
		<p>No categories found</p> 
		@elseif(count($genres) == 1)
			<p>1 category found</p> 
			<br>
			<p>Click on the category name to view a description and all books in that category</p> 
			@else
				<p>{{{count($genres)}}} categories found</p> 
				<br>
				<p>Click on the category name to view a description and all books in that category</p> 
	@endif
	<br>
	<section class="booklist">
	<table> 
		<thead>
		    <tr>
				<td>Name</td>
				<td>Number of books</td>
			</tr>
		</thead>
 
 		<tbody>
		@foreach($genres as $genre)
			<tr>
				<td><a href="{{{URL::to('genre')}}}/{{{$genre->id}}}">{{{$genre->name}}}</a></td>
				<td>{{{count($genre->books)}}}</td>
			</tr>
		@endforeach
		</tbody>
		
	</table>
	</section>
	<br/>
	@stop