<?php

namespace settings\repository\impl;

use settings\repository\GlobalSettingRepository;
use settings\mapper\GlobalSettingRowMapper;
use Illuminate\Support\Facades\DB;
// uses the rowmapper class- dependency injection here.
use settings\mapper\RowMapper;
use settings\GlobalSetting;

class DbGlobalSettingRepository implements GlobalSettingRepository {
	
	private $rowMapper;
	
	//constructor for globalSettingRowMapper
	function __construct() {
		$this->rowMapper = new GlobalSettingRowMapper ();
	}
	
	// this method is passed an object and converts it to an array to update the db with
	function setSettings($globalSetting) {
		
		$localGlobalSetting = new GlobalSetting ();
		$localGlobalSetting = $globalSetting;
		
		DB::table ( 'globalsettings' )->update ( array (
				'permissable_loan_period' => $localGlobalSetting->getPermissableLoanPeriod (),
				'loan_rate' => $localGlobalSetting->getLoanRate (),
				'global_book_allowance_under1yr' => $localGlobalSetting->getGlobalBookAllowanceUnder1yr (),
				'global_book_allowance_over1yr' => $localGlobalSetting->getGlobalBookAllowanceOver1yr (),
				'show_most_borrowed' => $localGlobalSetting->getShowMostBorrowed(),
				'show_highest_rated' => $localGlobalSetting->getShowHighestRated()
		) );
	}
	
	 /* this method gets the globalSettings from the db 
	  *  and passes them into a globalsetting object
	  *  for return*/
	function getSettings() {
		
		$globalSetting = DB::table ( 'globalsettings' )->get ();
		return $this->rowMapper->mapRowToObject ( $globalSetting );
	}
}