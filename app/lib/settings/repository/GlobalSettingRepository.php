<?php

namespace settings\repository;

// a hook or interface for the Global settings service
interface GlobalSettingRepository {
	
	public function getSettings();
	public function setSettings($settings);
}