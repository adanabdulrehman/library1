<?php

namespace settings\mapper;

// a hook for the framework
interface RowMapper {
	
	public function mapRow($row);
}
