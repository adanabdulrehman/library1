<?php
// a namespace was set up so as to make the library visible
namespace settings;

// a pojo
class GlobalSetting {
	private $id;
	private $permissableLoanPeriod;
	private $loanRate;
	private $globalBookAllowanceUnder1yr;
	private $globalBookAllowanceOver1yr;
	private $showMostBorrowed;
	private $showHighestRated;
	
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
	}
	
	public function getShowHighestRated() {
		return $this->showHighestRated;
	}
	
	public function setShowHighestRated($showHighestRated) {
		$this->showHighestRated = $showHighestRated;
	}
	
	public function getShowMostBorrowed() {
		return $this->showMostBorrowed;
	}
	public function setShowMostBorrowed($showMostBorrowed) {
		$this->showMostBorrowed = $showMostBorrowed;
	}
	
	public function getPermissableLoanPeriod() {
		return $this->permissableLoanPeriod;
	}
	public function setPermissableLoanPeriod($permissableLoanPeriod) {
		$this->permissableLoanPeriod = $permissableLoanPeriod;
	}
	public function getLoanRate() {
		return $this->loanRate;
	}
	public function setLoanRate($loanRate) {
		$this->loanRate = $loanRate;
	}
	public function getGlobalBookAllowanceUnder1yr() {
		return $this->globalBookAllowanceUnder1yr;
	}
	public function setGlobalBookAllowanceUnder1yr($globalBookAllowanceUnder1yr) {
		$this->globalBookAllowanceUnder1yr = $globalBookAllowanceUnder1yr;
	}
	public function getGlobalBookAllowanceOver1yr() {
		return $this->globalBookAllowanceOver1yr;
	}
	public function setGlobalBookAllowanceOver1yr($globalBookAllowanceOver1yr) {
		$this->globalBookAllowanceOver1yr = $globalBookAllowanceOver1yr;
	}
}
