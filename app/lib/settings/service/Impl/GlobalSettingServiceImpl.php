<?php

namespace settings\service\impl;

use settings\service\GlobalSettingService;
// uses the repository class dependency injection happens.
use settings\repository\GlobalSettingRepository;

class GlobalSettingServiceImpl implements GlobalSettingService {
	
	private $globalSettingRepository;
	
	// constructor for injection of repository.
	function __construct(GlobalSettingRepository $globalSettingRepository) {
		$this->globalSettingRepository = $globalSettingRepository;
	}
	
	// passes the parameters from the view through the controller to here to the repository
	function setSettings($globalSetting) {
		$this->globalSettingRepository->setSettings($globalSetting);
	}
	
	// gets the settings and passes them to the controller
	function getSettings() {
		return $this->globalSettingRepository->getSettings();
	}
}
